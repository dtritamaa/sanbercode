@extends('layout.master')

@section('title')
    Tambah Cast
@endsection

@section('content')

<form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama : </label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Umur : </label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umurnya">
                @error('umur')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Biodata : </label>
                <input type="text" class="form-control" name="biodata" id="biodata" placeholder="Masukkan Biodata">
                @error('biodata')
                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                          {{ $message }}  
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah Cast !</button>
        </form>

@endsection