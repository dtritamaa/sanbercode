<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'biodata' => 'required',
        ]);

        Cast::create([
            "nama" => $request -> nama,
            "umur" => $request -> umur,
            "biodata" => $request-> biodata,
            
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $listcast = Cast::all();
        return view('cast.index', compact('listcast')); 
    }
    
    public function show($id)
    {
        $cast = Cast::find($id);
        return view('cast.show', compact('cast')); 
    }

    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('cast.edit', compact('cast')); 
    }
    
    public function update( $id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'biodata' => 'required',
        ]);

        $cast = Cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->biodata = $request->biodata;
        $cast->update();
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }

}
