<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/send" method="POST">
    @csrf
        <label>First Name : </label>
        <br>
            <br>
                <input type="text" name="name1">
            </br>
        </br>
        <label>Last Name : </label>
        <br>
            <br>
                <input type="text" name="name2">
            </br>
        </br>

        <label>Gender :</label> <br>
        <br>
            <input type="radio" name="gender">Male <br>
            <input type="radio" name="gender">Female <br>
            <input type="radio" name="gender">Other <br>
        </br>

        <label>Nationality : </label>
        <br>
            <br>
            <select name="nationality">
                <option value="1">Indonesia</option>
                <option value="2">English</option>
                <option value="3">Japan</option>
                <option value="4">Dutch</option>
                <option value="5">Italy</option>
            </select>
            <br>
        </br>

        <label>Language Spoken : </label>
        <br>
            <br>
                <input type="checkbox" name="language">Bahasa Indonesia <br>
                <input type="checkbox" name="language">English <br>
                <input type="checkbox" name="language">Japan <br>
                <input type="checkbox" name="language">Other
            </br>
        </br>

        <label>Bio : </label>
        <br>
            <br>
                <textarea name="bio" cols="30" rows="10"></textarea>
            </br>
            <button type="submit">Sign Up</button>
        </br> 
    </body>
</html>