<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('home.form');
    }

    public function send(Request $request){
        $name1 = $request['name1'];
        $name2 = $request['name2'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];

        return view('home.welcome', compact('name1', 'name2', 'gender', 'nationality', 'language', 'bio'));
    }
}
